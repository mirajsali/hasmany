# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

specialists = Specialist.create ([{name: 'Thomas Sekula', specialty:'Neurology'}])
specialists = Specialist.create ([{name: 'Derek Jay', specialty:'Orthopaedics'}])
specialists = Specialist.create ([{name: 'Wendy Beadles', specialty:'Cardiology'}])
specialists = Specialist.create ([{name: 'Anita Gillani', specialty:'General'}])
specialists = Specialist.create ([{name: 'Michael Sanders', specialty:'Radiology'}])
specialists = Specialist.create ([{name: 'Adrian Wojernowzki', specialty:'General'}])
specialists = Specialist.create ([{name: 'Tony Gates', specialty:'Cardiology'}])
specialists = Specialist.create ([{name: 'Neel Kanji', specialty:'Endocrinology'}])
specialists = Specialist.create ([{name: 'Raheel Ali', specialty:'Radiology'}])
specialists = Specialist.create ([{name: 'Lucas Scott', specialty:'General'}])
specialists = Specialist.create ([{name: 'James Cobb', specialty:'Cardiology'}])
specialists = Specialist.create ([{name: 'Jeremy Fitzgerald', specialty:'Oncology'}])
specialists = Specialist.create ([{name: 'Juan Gonzalez', specialty:'Anesthesiology'}])
specialists = Specialist.create ([{name: 'Eric Aybar', specialty:'General'}])
specialists = Specialist.create ([{name: 'Michael Smith', specialty:'Cardiology'}])

insurances = Insurance.create ([{name: 'Life Now', street_address: '1200 Oregon Trl'},
                                {name: 'Mankind', street_address: '1422 Century Ave'},
                                {name: 'Ensura', street_address: '1202 Osceola Trl'},
                                {name: 'One Life', street_address: '951 Jackson Rd'},
                                {name: 'Health Shield', street_address: '192 Corporate Dr'}])

patients = Patient.create ([{name:'Jane Smith', street_address:'1313 Seafoam Rd',:insurance_id => 1},
                            {name:'Jame McAdoo', street_address:'16302 El Dorado Blvd',:insurance_id => 3},
                            {name:'Benjamin Greene', street_address:'4122 Oak Hill Dr',:insurance_id => 5},
                            {name:'Amin Hirji', street_address:'3124 Maple Hill St',:insurance_id => 2},
                            {name:'Sung Ho', street_address:'934 Avalanche Ave',:insurance_id => 4},
                            {name:'Jake Sully', street_address:'113 Marina Dr',:insurance_id => 2},
                            {name:'Reggie Jackson', street_address:'7233 Mulan St',:insurance_id => 3}])

appointments = Appointment.create ([{specialist_id: 9, patient_id: 6,complaint:'I need to get an x-ray for my forearm. I may have broken it.',appointment_date:'2014-01-13', fee:'50.00'},
                                    {specialist_id: 2, patient_id: 2,complaint:'I need to get surgery for my knee.', appointment_date:'2014-01-21',fee:'80.00'},
                                    {specialist_id: 2, patient_id: 4,complaint:'I injured my knee playing football. I cannot put any weight on my right leg.',appointment_date:'2014-02-11',fee:'50.00'},
                                    {specialist_id: 14, patient_id: 7,complaint:'I am having problems sleeping.',appointment_date:'2014-02-22',fee:'60.00'},
                                    {specialist_id: 4, patient_id: 5,complaint:'I have had a flu for two weeks now.',appointment_date:'2014-03-19',fee:'50.00'},
                                    {specialist_id: 4, patient_id: 4,complaint:'I have had a flu for two weeks now.',appointment_date:'2014-03-21',fee:'50.00'},
                                    {specialist_id: 12, patient_id: 7,complaint:'',appointment_date:'2014-04-10',fee:'90.00'},
                                    {specialist_id: 9, patient_id: 2,complaint:'I injured my arm and the pain is high.',appointment_date:'2014-05-10',fee:'50.00'},
                                    {specialist_id: 6, patient_id: 6,complaint:'I have been coughing a lot and it hurts.',appointment_date:'2014-06-07',fee:'75.00'},
                                    {specialist_id: 7, patient_id: 3,complaint:'',appointment_date:'2014-07-01',fee:'50.00'},
                                    {specialist_id: 4, patient_id: 6,complaint:'My allergies are affecting me badly.',appointment_date:'2014-07-25',fee:'40.00'},
                                    {specialist_id: 11, patient_id: 7,complaint:'Having issues with light physical activities.',appointment_date:'2014-08-05',fee:'75.00'},
                                    {specialist_id: 15, patient_id: 1,complaint:'I have aches in my chest.',appointment_date:'2014-09-28',fee:'80.00'},
                                    {specialist_id: 3, patient_id: 5,complaint:'I am having some pain in my chest.',appointment_date:'2014-09-29',fee:'75.00'}])